package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt" // пакет для форматированного ввода вывода
	"github.com/nats-io/go-nats"
	"io/ioutil"
	"log"      // пакет для логирования
	"net/http" // пакет для поддержки HTTP протокола
	"net/http/httputil"
	"strings"
	"time"
)

func RouterHandler(w http.ResponseWriter, r *http.Request) {
	if !validation(r) {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	defer r.Body.Close()

	dumped, _ := httputil.DumpRequest(r, true)
	strResp, _ := json.Marshal(map[string]string{
		"request":    string(dumped),
		"remoteAddr": r.RemoteAddr,
	})

	respNats, _ := sendMsg(strResp)

	b := bufio.NewReader(strings.NewReader(string(respNats.Data)))
	resp, _ := http.ReadResponse(b, r)

	body, _ := ioutil.ReadAll(resp.Body)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(resp.StatusCode)
	fmt.Fprintf(w, string(body)) // отправляем данные на клиентскую сторону
}

func main() {
	http.HandleFunc("/", RouterHandler) // установим роутер

	err := http.ListenAndServe(":9000", nil) // задаем слушать порт
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func sendMsg(msg []byte) (*nats.Msg, error) {
	urls := nats.DefaultURL
	userCreds := ""

	// Connect Options.
	opts := []nats.Option{nats.Name("NATS Sample Requestor")}

	// Use UserCredentials
	if userCreds != "" {
		opts = append(opts, nats.UserCredentials(userCreds))
	}

	// Connect to NATS
	nc, err := nats.Connect(urls, opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()
	subj, payload := "AUTOTEKA", msg

	resp, err := nc.Request(subj, payload, 2*time.Second)
	if err != nil {
		if nc.LastError() != nil {
			log.Fatalf("%v for request", nc.LastError())
		}
		log.Fatalf("%v for request", err)
	}

	return resp, err
}

var fieldMap = map[string]map[string]bool{
	"/api/auth":         {"key": true, "client": true},
	"/api/auth_refresh": {"refresh_token": true, "client": true},
	"/api/preview":      {"token": true, "vin": true},
	"/api/report":       {"token": true, "preview_id": true},
	"/api/report-fill":  {"token": true, "preview_id": true},
	"/api/report-list":  {"token": true},
}

func validation(r *http.Request) bool {
	var data map[string]interface{}
	b, err := ioutil.ReadAll(r.Body)
	r.Body = ioutil.NopCloser(bytes.NewBuffer(b))

	if err != nil {
		return false
	}
	err = json.Unmarshal(b, &data)
	if err != nil {
		return false
	}

	fields, ok := fieldMap[r.URL.Path]

	if !ok || len(data) == 0 {
		return false
	}

	findFields := 0
	for key, _ := range data {
		if _, ok := fields[key]; ok {
			findFields++
		}
	}

	return len(fields) <= findFields
}
