package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"github.com/nats-io/go-nats"
	"log"
	"net/http"
	"net/http/httputil"
	"runtime"
	"strings"
	"time"
)

// NOTE: Can test with demo servers.
// nats-rply -s demo.nats.io <subject> <response>
// nats-rply -s demo.nats.io:4443 <subject> <response> (TLS version)

func usage() {
	log.Printf("Usage: nats-rply [-s server] [-creds file] [-t] <subject> <response>\n")
	flag.PrintDefaults()
}

func printMsg(m *nats.Msg, i int) {
	log.Printf("[#%d] Received on [%s]: '%s'\n", i, m.Subject, string(m.Data))
}

func main() {
	//resp, err := http.Post("", "application/json", &buf)
	//resp, err := http.Get("http://172.17.0.3:8080/")
	//if err != nil {
	//	log.Fatalln(err)
	//}
	//
	//body, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	log.Fatalln(err)
	//}
	//
	//log.Println(string(body))

	var urls = "nats://127.0.0.1:4222"
	//var userCreds = flag.String("creds", "", "User Credentials File")
	//var showTime = flag.Bool("t", false, "Display timestamps")
	//
	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()

	// Connect Options.
	opts := []nats.Option{nats.Name("NATS Sample Responder")}
	opts = setupConnOptions(opts)

	//// Use UserCredentials
	//if *userCreds != "" {
	//	opts = append(opts, nats.UserCredentials(*userCreds))
	//}

	// Connect to NATS
	nc, err := nats.Connect(urls, opts...)
	if err != nil {
		log.Fatal(err)
	}

	nc.Subscribe("AUTOTEKA", func(msg *nats.Msg) {
		nc.Publish(msg.Reply, backReq(msg))
	})
	nc.Flush()

	if err := nc.LastError(); err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on [%s]", "AUTOTEKA")

	runtime.Goexit()
}

func setupConnOptions(opts []nats.Option) []nats.Option {
	totalWait := 10 * time.Minute
	reconnectDelay := time.Second

	opts = append(opts, nats.ReconnectWait(reconnectDelay))
	opts = append(opts, nats.MaxReconnects(int(totalWait/reconnectDelay)))
	opts = append(opts, nats.DisconnectHandler(func(nc *nats.Conn) {
		log.Printf("Disconnected: will attempt reconnects for %.0fm", totalWait.Minutes())
	}))
	opts = append(opts, nats.ReconnectHandler(func(nc *nats.Conn) {
		log.Printf("Reconnected [%s]", nc.ConnectedUrl())
	}))
	opts = append(opts, nats.ClosedHandler(func(nc *nats.Conn) {
		log.Fatal("Exiting, no servers available")
	}))
	return opts
}

func backReq(m *nats.Msg) []byte {
	var msgDecode map[string]string
	err := json.Unmarshal(m.Data, &msgDecode)
	if err != nil {
		return []byte(err.Error())
	}

	b := bufio.NewReader(strings.NewReader(msgDecode["request"]))

	req, err := http.ReadRequest(b)

	req.URL.Host = "127.0.0.1:8080"
	req.Host = ""
	req.URL.Scheme = "http"
	req.RequestURI = ""

	resp, err := http.DefaultClient.Do(req)
	defer resp.Body.Close()

	respB, err := httputil.DumpResponse(resp, true)

	return respB
}
